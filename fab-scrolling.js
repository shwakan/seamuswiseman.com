$("a#fab").click(function scroll() {
	if ($("#parallax").css("perspective") == "1px") {
		$("#content").velocity("scroll", {container: $("#parallax"), duration: 1200}, [0.95, 0.05, 0.795, 0.035]);
	}
	else {
		setTimeout(function() {
			$("#content").velocity("scroll", {container: $("#parallax"), duration: 1200}, [0.95, 0.05, 0.795, 0.035]);
		}, 600);	
	}
});
